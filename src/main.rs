#![warn(clippy::all, clippy::pedantic)]
//! GT is a graphical terminal emulator written in Rust using the gtk3 and vte
//! libraries.
use clap::{crate_version, load_yaml, App};
extern crate gdk;
extern crate gio;
extern crate glib;
extern crate gtk;

mod config;
use config::{Config, RuntimeConfig};
mod gui;

#[macro_use]
extern crate lazy_static;

lazy_static! {
    static ref CONFIG: Config = Config::new();
    static ref RUNTIME: RuntimeConfig = {
        let yaml = load_yaml!("cli.yaml");
        let args = App::from(yaml).version(crate_version!()).get_matches();
        RuntimeConfig::new(&args)
    };
}

fn main() {
    gui::run();
}
