#![warn(clippy::all, clippy::pedantic)]
use clap::ArgMatches;
use nix::unistd;
use nix::unistd::{Uid, User};
use serde::Deserialize;
use xdg_basedir::*;

use std::fs;
use std::path::PathBuf;
use std::{env, process};

#[derive(Deserialize, Debug)]
pub struct Config {
    pub user: String,
    pub shell: PathBuf,
    pub hostname: String,
    pub startup_dir: String,
}

#[derive(Deserialize, Debug)]
#[cfg(target_pointer_width = "64")]
pub struct Geometry {
    pub x: i64,
    pub y: i64,
}

#[derive(Deserialize, Debug)]
#[cfg(target_pointer_width = "32")]
pub struct Geometry {
    pub x: i32,
    pub y: i32,
}

#[derive(Deserialize, Debug)]
#[cfg(target_pointer_width = "64")]
pub struct RuntimeConfig {
    pub command: Option<PathBuf>,
    pub directory: Option<String>,
    pub lines: Option<i64>,
    pub geometry: Option<Geometry>,
}

#[derive(Deserialize, Debug)]
#[cfg(target_pointer_width = "32")]
pub struct RuntimeConfig {
    pub command: Option<PathBuf>,
    pub directory: Option<String>,
    pub lines: Option<i32>,
    pub geometry: Option<Geometry>,
}

#[derive(Deserialize, Debug)]
#[cfg(target_pointer_width = "64")]
pub struct UserConfig {
    pub scrollback_lines: Option<i64>,
    pub show_tabs: Option<bool>,
}

#[derive(Deserialize, Debug)]
#[cfg(target_pointer_width = "32")]
pub struct UserConfig {
    pub scrollback_lines: Option<i32>,
    pub show_tabs: Option<bool>,
}

impl Config {
    fn get_userdata() -> (String, PathBuf) {
        if let Ok(Some(user)) = User::from_uid(Uid::current()) {
            (user.name, user.shell)
        } else {
            (String::from("unknown"), PathBuf::from("/bin/sh"))
        }
    }

    fn get_hostname() -> String {
        let mut buf = [0u8; 64];
        let hostname_cstr = unistd::gethostname(&mut buf).expect("Failed getting hostname");
        hostname_cstr.to_str().unwrap_or("unknown").to_string()
    }

    fn get_startup_dir() -> String {
        let path = if let Ok(dir) = env::current_dir() {
            dir
        } else {
            home::home_dir().unwrap_or(PathBuf::from("/"))
        };
        String::from(path.to_str().unwrap_or("/"))
    }

    pub fn new() -> Config {
        let user_data = Config::get_userdata();
        Config {
            user: user_data.0,
            shell: user_data.1,
            hostname: Config::get_hostname(),
            startup_dir: Config::get_startup_dir(),
        }
    }
}

impl UserConfig {
    pub fn get_config_dir() -> PathBuf {
        let mut configdir: PathBuf = match get_config_home() {
            Ok(c) => c,
            Err(e) => {
                eprintln!("{}", e);
                process::exit(1);
            }
        };
        let progname = env!("CARGO_PKG_NAME");
        configdir.push(progname);
        configdir
    }

    pub fn new() -> UserConfig {
        let mut config: PathBuf = UserConfig::get_config_dir();
        config.push("config.toml");
        let config = if config.exists() {
            match fs::read_to_string(config) {
                Ok(c) => c,
                Err(e) => {
                    eprintln!("{}", e);
                    process::exit(1);
                }
            }
        } else {
            include_str!("config.toml").to_string()
        };
        let userconfig: UserConfig = match toml::from_str(&config) {
            Ok(c) => c,
            Err(e) => {
                eprintln!("{}", e);
                process::exit(1);
            }
        };
        userconfig
    }
}

impl RuntimeConfig {
    pub fn new(args: &ArgMatches) -> RuntimeConfig {
        let command = if let Some(c) = args.value_of("command") {
            Some(PathBuf::from(c))
        } else {
            None
        };
        let directory = if let Some(c) = args.value_of("directory") {
            Some(String::from(c))
        } else {
            None
        };
        let lines = if let Some(c) = args.value_of("scrollback_lines") {
            if let Ok(num) = c.trim().parse() {
                Some(num)
            } else {
                None
            }
        } else {
            None
        };
        let geometry = if let Some(geo) = args.value_of("geometry") {
            let split: Vec<&str> = geo.split('x').collect();
            if split.len() == 2 {
                Some(Geometry {
                    x: split[0].trim().parse().unwrap_or(80),
                    y: split[1].trim().parse().unwrap_or(25),
                })
            } else {
                None
            }
        } else {
            None
        };

        RuntimeConfig {
            command,
            directory,
            lines,
            geometry,
        }
    }
}
