#![warn(clippy::all, clippy::pedantic)]
use crate::gtk::WidgetExt;
use crate::gui::Gui;
use std::rc::Rc;

//const ESC: u16 = 9;
//const TAB: u16 = 23;
//const COLON: u16 = 47;
//const PAGE_UP: u16 = 112;
//const PAGE_DOWN: u16 = 117;
//const UP: u16 = 111;
//const DOWN: u16 = 116;
const LEFT: u16 = 113;
const RIGHT: u16 = 114;
//const HOME: u16 = 110;
//const END: u16 = 115;
//const Q: u16 = 24;
//const W: u16 = 25;
//const E: u16 = 26
//const R: u16 = 27;
const T: u16 = 28;
//const Y: u16 = 29;
//const U: u16 = 30;
//const I: u16 = 31;
//const O: u16 = 32;
//const P: u16 = 33;
//const A: u16 = 38;
//const S: u16 = 39;
//const D: u16 = 40;
//const F: u16 = 41;
//const G: u16 = 42;
//const H: u16 = 43;
//const J: u16 = 44;
//const K: u16 = 45;
//const L: u16 = 46;
//const Z: u16 = 52;
//const X: u16 = 53;
const C: u16 = 54;
const V: u16 = 55;
//const B: u16 = 56;
//const N: u16 = 57;
//const M: u16 = 58;
const ONE: u16 = 10;
const TWO: u16 = 11;
const THREE: u16 = 12;
const FOUR: u16 = 13;
const FIVE: u16 = 14;
const SIX: u16 = 15;
const SEVEN: u16 = 16;
const EIGHT: u16 = 17;
const NINE: u16 = 18;
const ZERO: u16 = 19;

pub struct Key {
    pub key: u16,
    pub ctrl: bool,
    pub shift: bool,
    pub alt: bool,
}

impl Key {
    pub fn process_keypress(&self, gui: &Rc<Gui>) {
        if self.ctrl && self.shift && !self.alt {
            // Hack to keep from passing keypresses through to the terminal
            if let Some(term) = gui.get_current_term() {
                term.set_sensitive(false);
            }
            // Ctrl - Shift
            match self.key {
                T => {
                    gui.new_tab(&None);
                    {}
                }
                C => gui.copy_text(),
                V => gui.paste_text(),
                _ => {}
            };
        } else if self.ctrl && !self.shift && !self.alt {
            // Ctrl
            match self.key {
                LEFT => gui.prev_tab(),
                RIGHT => gui.next_tab(),
                _ => {}
            };
        } else if !self.ctrl && !self.shift && self.alt {
            // Alt
            match self.key {
                ONE => gui.nth_tab(0),
                TWO => gui.nth_tab(1),
                THREE => gui.nth_tab(2),
                FOUR => gui.nth_tab(3),
                FIVE => gui.nth_tab(4),
                SIX => gui.nth_tab(5),
                SEVEN => gui.nth_tab(6),
                EIGHT => gui.nth_tab(7),
                NINE => gui.nth_tab(8),
                ZERO => gui.nth_tab(9),
                _ => {}
            };
        }
    }
}
