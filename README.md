# GT Terminal Emulator
Simple (for now) terminal emulator in Rust using vte-rs and gtk-rs. Tabbed
interface, keyboard driven. Few configuration options or eye candy. The scope of
gt is to produce a lightweight and productive terminal environment, not to build
a showcase for screenshots to post in forums.

## Keybindings
* Ctrl/D - logout
* Ctrl/Left - previous tab
* Ctrl/Right - next tab
* Alt/<number> - switch to <number> tab
* Ctrl/Shift/T - new tab

## Planned features
| Feature | Priority | Finished |
| --- | --- | --- |
| Copy/paste | high | 3/2021 |
| Set scrollback lines | high | 3/2021 |
| Split panes | high | no |
| Run program from commandline | high | 3/2021 |
| Color settings | low | no |

## Unplanned features
* Configuration dialog - configuration is via config.toml
* clickable links
