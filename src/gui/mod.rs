#![warn(clippy::all, clippy::pedantic)]
use crate::gdk::ModifierType;
use crate::glib::clone;
use crate::gtk::WindowType::Toplevel;
use crate::gtk::{prelude::*, Button, ContainerExt, IconSize, Inhibit, Label,
        NotebookExt, Orientation, ReliefStyle, WidgetExt};
use vte::{PtyFlags, Terminal, TerminalExt};

use std::path::{Path, PathBuf};
use std::rc::Rc;

mod keys;
use keys::Key;
use crate::{CONFIG, RUNTIME};

pub struct Gui {
    window: gtk::Window,
    notebook: gtk::Notebook,
}

impl Gui {
    fn new() -> Gui {
        Gui {
            window: gtk::Window::new(Toplevel),
            notebook: gtk::Notebook::new(),
        }
    }

    fn label_tab(&self, tab: Terminal, text: String) {
        let label_box = gtk::Box::new(Orientation::Horizontal, 10);
        let label = Label::new(Some(&text));
        let close_button = Button::from_icon_name(Some("window-close"), IconSize::Menu);
        close_button.set_property_margin(0);
        close_button.set_relief(ReliefStyle::None);
        close_button.set_tooltip_text(Some("Close Tab"));
        close_button.set_can_focus(false);
        label_box.add(&label);
        label_box.add(&close_button);
        label_box.show_all();
        self.notebook.set_tab_label(&tab, Some(&label_box));
        let notebook = self.notebook.clone();
        close_button.connect_clicked(clone!(@weak notebook => move |_| {
            notebook.remove(&tab);
        }));
    }


    pub fn new_tab(&self, cmd: &Option<PathBuf>) -> Terminal {
        let term = Terminal::new();
        let startup_dir = RUNTIME.directory.as_ref().unwrap_or(&CONFIG.startup_dir);
        let command = cmd.as_ref().unwrap_or(&CONFIG.shell);
        let lines = &RUNTIME.lines.unwrap_or(500);
        term.spawn_sync(
            PtyFlags::DEFAULT,
            Some(startup_dir),
            &[&command],
            &[Path::new("TERM=xterm")],
            glib::SpawnFlags::DEFAULT,
            Some(&mut || {}),
            Some(&gio::Cancellable::new()),
        )
        .unwrap();
        term.show();
        term.set_scrollback_lines(*lines);
        self.notebook.add(&term);
        self.notebook.set_tab_reorderable(&term, true);
        let tab_title = format!("{}@{}", CONFIG.user, CONFIG.hostname);
        term.connect_child_exited(clone!(@weak term => move |_,_| {
            unsafe { term.destroy(); }
        }));
        self.label_tab(term.clone(), tab_title);
        term
    }
    pub fn next_tab(&self) {
        self.notebook.next_page();
    }
    pub fn prev_tab(&self) {
        self.notebook.prev_page();
    }
    pub fn nth_tab(&self, num: i32) {
        self.notebook.set_property_page(num);
    }
    pub fn get_current_term(&self) -> Option<vte::Terminal> {
        let tab = self.notebook.get_current_page();
        let widget = match self.notebook.get_nth_page(tab) {
            Some(c) => c,
            None => return None,
        };
        if widget.clone().upcast::<gtk::Widget>().is::<vte::Terminal>() {
            Some(widget.downcast::<vte::Terminal>().unwrap())
        } else {
            None
        }
    }
    pub fn copy_text(&self) {
        if let Some(term) = self.get_current_term() {
            term.copy_primary();
        }
    }
    pub fn paste_text(&self) {
        if let Some(term) = self.get_current_term() {
            term.paste_primary();
        }
    }
}

pub fn run() {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }
    let gui = Rc::new(Gui::new());
    gui.notebook.set_scrollable(true);
    gui.notebook.set_scrollable(true);
    gui.window.add(&gui.notebook);
    gui.window.set_default_size(800, 600);
    gui.window.show_all();
    let term = gui.new_tab(&RUNTIME.command);

    let provider = gtk::CssProvider::new();
    provider
        .load_from_data(format!(
            "notebook tab button {{ min-height: 0; min-width: 0; padding: 0px; margin-top: 0px; margin-bottom: 0px; }}")
            .as_bytes())
        .expect("Failed to load CSS");

    gtk::StyleContext::add_provider_for_screen(
        &gdk::Screen::get_default().expect("Error initializing gtk css provider."),
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    gui.notebook.set_show_tabs(false);
    term.grab_focus();
    if let Some(geometry) = &RUNTIME.geometry {
        term.set_size(geometry.x, geometry.y);
    } else {
        term.set_size(80, 25);
    }

    gui.notebook
        .connect_page_removed(clone!(@weak gui => move |_,_,_| {
            match gui.notebook.get_children().len() {
                0 => gtk::main_quit(),
                1 => gui.notebook.set_show_tabs(false),
                _ => gui.notebook.set_show_tabs(true),
            };
        }));

    gui.notebook
        .connect_page_added(clone!(@weak gui => move |_,_,_| {
            if gui.notebook.get_children().len() > 1 {
                gui.notebook.set_show_tabs(true);
            }
        }));

    let clone = gui.clone();
    gui.window.connect_key_press_event(move |_, gdk| {
        let key = Key {
            key: gdk.get_keycode().unwrap(),
            ctrl: gdk.get_state().contains(ModifierType::CONTROL_MASK),
            shift: gdk.get_state().contains(ModifierType::SHIFT_MASK),
            alt: gdk.get_state().contains(ModifierType::MOD1_MASK),
        };
        key.process_keypress(&clone.clone());
        Inhibit(false)
    });

    let clone = gui.clone();
    gui.window.connect_key_release_event(move |_, _| {
        if let Some(term) = clone.get_current_term() {
            term.set_sensitive(true);
            term.grab_focus();
        }
        Inhibit(false)
    });

    gui.window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    gtk::main()
}
